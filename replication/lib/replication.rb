# frozen_string_literal: true

require_relative "replication/version"
require_relative "replication/coordinator/coordinator"
require_relative "replication/server/server"

module Replication
  class Error < StandardError; end
end
