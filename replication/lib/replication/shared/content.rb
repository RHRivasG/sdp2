class Content
  attr_accessor :content
  attr_reader :ts

  def initialize()
    @ts = Time.now.to_i
  end

  def to_json(*args)
    {
      content: content,
      ts: ts
    }.to_json(*args)
  end

  def []=(key, value)
    return @ts = value if key.to_s == "ts"
    return @content = value if key.to_s == "content"
  end
end
