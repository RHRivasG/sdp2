require 'socket'
require 'json'
require 'logger'

class Server
  LOGGER = Logger.new(STDOUT)

  def initialize(port, filename)
    @port = port
    @filename = filename
  end

  def run
    server = TCPServer.new(@port)
    LOGGER.info("Servidor tcp iniciado en #{@port}")
    Thread.start(server.accept) do |client|
      LOGGER.debug("Conectado a #{client.remote_address.ip_address}")
      while msg = client.gets.chomp
        LOGGER.debug("Recibido: #{msg}")
        if msg == "VOTE_REQUEST" && rand <= 0.5
          LOGGER.info("Comprometiendose a replicar")
          client.puts "VOTE_COMMIT"
        elsif msg == "VOTE_REQUEST"
          LOGGER.warn("Abortando replicacion solicitada")
          client.puts "VOTE_ABORT"
        elsif msg == "GLOBAL_ABORT"
          LOGGER.warn("Replicacion abortada")
        elsif msg == "RESTORE"
          content = IO.read(@filename)
          client.puts content
          LOGGER.info("Enviando contenidos de archivo: #{content}")
        elsif (result = JSON.parse(msg)) && result.has_key?("content")
          LOGGER.info("Guardando contenido: #{result}")
          IO.write(@filename, msg)
        end
      end
      LOGGER.debug("Finalizando conexion con #{client.remote_address.ip_address}")
    end while !server.closed?
  end
end

Server.new(3500, "rep.xml").run
