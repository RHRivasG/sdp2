require_relative "./server_link"

class Servers
  def initialize(servers = [])
    @servers = servers
  end

  def <<(server)
    return @servers << server if server.is_a? ServerLink

    if server.is_a? String
      host, port = server.split(/:/)
      @servers << ServerLink.new(host, port.to_i)
    end
  end

  def replicate?
    @servers.all?(&:replicate?)
  end

  def commit(content)
    @servers.each { |server| server.commit(content) }
  end

  def abort
    @servers.each(&:abort)
  end

  def restore
    @servers
      .map(&:restore)
      .max_by(&:ts)
  end
end
