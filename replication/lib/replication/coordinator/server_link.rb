require 'socket'
require 'json'
require_relative '../shared/content'

class ServerLink
  def initialize(addr, port = 3500)
    @addr = addr
    @port = port
  end

  def replicate?
    connect

    @socket.puts "VOTE_REQUEST"
    @socket.gets.chomp == "VOTE_COMMIT"
  rescue
    false
  end

  def commit(content)
    connect

    msg = Content.new
    msg.content = content

    @socket.puts JSON.dump(msg)
  end

  def abort
    connect

    Coordinator::LOGGER.debug("Enviando trama de replicacion")
    @socket.puts "GLOBAL_ABORT"
  end

  def restore
    connect

    @socket.puts "RESTORE"
    content = @socket.gets.chomp
    JSON.parse(content, object_class: Content)
  end

  private

  def connect
    times = 0
    begin
      @socket = TCPSocket.new(@addr, @port)
      Coordinator::LOGGER.warn("Intento #{times} de conexion a #{@addr}:#{@port}") if times > 0
      IO.select(nil, [@socket], nil, 5)
    rescue
      times += 1
      retry if times <= 2
      msg = "No se pudo conectar a #{@addr}:#{@port}, 3 intentos"
      Coordinator::LOGGER.error(msg)
      raise msg
    end
  end
end
