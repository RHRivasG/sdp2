require "socket"
require "logger"
require_relative "./servers"

class Coordinator
  LOGGER = Logger.new(STDOUT)

  def initialize
    @servers = Servers.new
    @servers << "127.0.0.1:3500"

    @port = 5000
  end

  def run
    server = TCPServer.new(@port)
    LOGGER.info("Servidor tcp iniciado en #{@port}")
    Thread.start(server.accept) do |connection|
      LOGGER.debug("Conectado a #{connection.remote_address.ip_address}")
      if (read = connection.gets.chomp) == "RESTORE"
        LOGGER.info("Iniciada restaurcacion")
        connection.puts JSON.dump(@servers.restore)
      else
        replication_request = JSON.parse(read)
        LOGGER.info("Enviando peticion de replicacion surgida de: #{replication_request}")
        if @servers.replicate?
          LOGGER.info("Iniciando replicacion con contenido: #{replication_request["content"]}")
          @servers.commit(replication_request["content"])
          LOGGER.info("Replicacion exitosa")
          connection.puts "Success"
        else
          LOGGER.warn("Abortando replicacion")
          @servers.abort
          LOGGER.warn("Replicacion abortada")
          connection.puts "Failure"
        end
      end
      LOGGER.debug("Finalizando conexion con #{connection.remote_address.ip_address}")
    end while !server.closed?
  end
end

Coordinator.new.run
