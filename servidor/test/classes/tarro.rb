require 'nokogiri'
require 'socket'

class Tarro
  include MonitorMixin

  def initialize()
    @consultor = new_cond
    @productor = new_cond
    @consumidor = new_cond
  end

  def llenar(actor)

    synchronize do

      @productor.wait

      time = Time.now
      prod_a = 60
      prod_b = 40

      unless File.exists? "data.xml"
        xml = Nokogiri::XML::Builder.new{ |xml|
          xml.tarro 
        }.to_xml

        File.write("data.xml", xml)
      end

      xml = Nokogiri::XML(File.open("data.xml")){ |x| x.noblanks }
      cant_a = value_of xml, "//totalA"
      cant_b = value_of xml, "//totalB"
      Nokogiri::XML::Builder.with(xml.at("//tarro")) { |xml|
        xml.transaccion do
          xml.accion "producir"
          xml.cantA prod_a
          xml.cantB prod_b
          xml.actor actor
          xml.fecha time.inspect 
          xml.totalA cant_a + prod_a
          xml.totalB cant_b + prod_b
        end
      }.to_xml
      File.write("data.xml", xml)

      @consultor.signal
      @consumidor.signal

      puts "exiting"
    end

  end

  def consumir(actor, cons_a, cons_b)

    synchronize do
      if cons_a and cons_b
        xml = Nokogiri::XML(File.open("data.xml")){ |x| x.noblanks }
        cant_a = value_of xml, "//totalA"
        cant_b = value_of xml, "//totalB"
        @consumidor.wait_while do
          xml = Nokogiri::XML(File.open("data.xml")){ |x| x.noblanks }
          cant_a = value_of xml, "//totalA"
          cant_b = value_of xml, "//totalB"
          result = cons_a > cant_a or cons_b > cant_b 
          @productor.signal if result
          result
        end
        Nokogiri::XML::Builder.with(xml.at("//tarro")) { |xml|
          xml.transaccion do
            xml.accion "consumir"
            xml.cantA cons_a
            xml.cantB cons_b
            xml.actor actor
            xml.fecha Time.now.inspect 
            xml.totalA cant_a - cons_a
            xml.totalB cant_b - cons_b
          end
        }.to_xml
        File.write("data.xml", xml)
      end
    end

  end

  def consultar(ts)
    if ts.nil?
      IO.read "data.xml"
    end
  end

  def respaldar
    data = Nokogiri::XML(File.open("data.xml"))
    socket = TCPSocket.new '192.168.0.103', 5000
    socket.puts Nokogiri::XML::Builder.new{ |xml|
      xml.Replicate(data) 
    }.to_xml
    socket.close
  end

  def restaurar
    socket = TCPSocket.new '192.168.0.103', 5000
    socket.puts Nokogiri::XML::Builder.new{ |xml|
      xml.Restore 
    }.to_xml
    sock_read, _, _ = IO.select [socket], nil, nil, nil
    sock_read[0].read
  end

  private 
  def value_of (xml, path)
    xml.xpath(path).children.count != 0 ? Integer(xml.xpath(path).children.last.text) : 0
  end
end
