    doc = Nokogiri::XML(File.open("data.xml"))
    doc.class # => Nokogiri::XML::Document
    @block = doc.xpath("//cantA")
    @block.class # => Nokogiri::XML::NodeSet
    @block.count # => 2
    @block.map { |node| p node.name }
    # => ["Item", "Item"]
    @block.map { |node| p node.class }
    # => [Nokogiri::XML::Element, Nokogiri::XML::Element]
    @block.map { |node| p node.children[0] }
    # => [19, 19]
    @block.map { |node| p node.class.superclass }
    # => [Nokogiri::XML::Node, Nokogiri::XML::Node]
    #
    #
    #@tarro.xpath('//tarro/accion').each do |node|
    #  node.add_next_sibling(accion)
    #end
    #
    #file = File.open("data.xml",'w')
    #file.puts @tarro.to_xml
    #file.close


    #@doc = Nokogiri::XML(File.open("data.xml"))
    #cant_a = @doc.xpath("//cantA").children.count
    #xml = Nokogiri::XML::Builder.new{ |xml|
    #  xml.body do 
    #    xml.tarro do
    #      xml.accion "producir"
    #      xml.cantidad "60"
    #      xml.tipo "A"
    #      xml.actor actor
    #      xml.fecha time.inspect 
    #      xml.cantA 50
    #      xml.cantB "0"
    #    end
    #  end
    #}.to_xml

    #File.write("data.xml", xml, mode:"a")

    #p cant_a
