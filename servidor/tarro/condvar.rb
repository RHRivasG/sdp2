require 'concurrent'

class CondVar
  def initialize
    @inner = Concurrent::Semaphore.new(0)
  end

  def wait(lock)
    lock.unlock
    @inner.acquire
    lock.lock
  end

  def signal
    @inner.release
  end
end
