require 'nokogiri'
require 'json'
require 'socket'
require 'drb/drb'
require 'logger'
require_relative 'condvar'

class Tarro
  include DRb::DRbUndumped
  LOGGER = Logger.new(STDOUT)

  def initialize()
    @consultor = CondVar.new
    @productor = CondVar.new
    # @productor_exists = Queue.new
    @consumidor = CondVar.new

    @lock = Mutex.new
  end

  def llenar(actor)

    # @productor_exists << actor

    @lock.synchronize do

      LOGGER.info("Producer waiting #{Thread.current}")

      @productor.wait(@lock)

      LOGGER.info("Producer woke up")

      unless File.exists? "data.xml"
        xml = Nokogiri::XML::Builder.new{ |xml|
          xml.tarro 
        }.to_xml

        File.write("data.xml", xml)
      end

      time = Time.now
      prod_a = 60
      prod_b = 40

      xml = Nokogiri::XML(File.open("data.xml")){ |x| x.noblanks }
      cant_a = value_of xml, "//totalA"
      cant_b = value_of xml, "//totalB"

      LOGGER.info("Producing")

      Nokogiri::XML::Builder.with(xml.at("//tarro")) { |xml|
        xml.transaccion do
          xml.accion "producir"
          xml.cantA prod_a
          xml.cantB prod_b
          xml.actor actor
          xml.fecha time.inspect 
          xml.totalA cant_a + prod_a
          xml.totalB cant_b + prod_b
        end
      }.to_xml
      File.write("data.xml", xml)

    end

    @consultor.signal
    LOGGER.info("Consumer signaled")
    @consumidor.signal

  end

  def consumir(actor, cons_a, cons_b)

    @lock.synchronize do

      unless File.exists? "data.xml"
        xml = Nokogiri::XML::Builder.new{ |xml|
          xml.tarro 
        }.to_xml

        File.write("data.xml", xml)
      end

      if cons_a and cons_b
        xml = Nokogiri::XML(File.open("data.xml")){ |x| x.noblanks }
        cant_a = value_of xml, "//totalA"
        cant_b = value_of xml, "//totalB"
        while cons_a > cant_a || cons_b > cant_b
          @productor.signal
          LOGGER.info("Producer signaled")
          LOGGER.info("Consumer waiting #{Thread.current}")
          @consumidor.wait(@lock)
          xml = Nokogiri::XML(File.open("data.xml")){ |x| x.noblanks }
          cant_a = value_of xml, "//totalA"
          cant_b = value_of xml, "//totalB"
        end
        Nokogiri::XML::Builder.with(xml.at("//tarro")) { |xml|
          xml.transaccion do
            xml.accion "consumir"
            xml.cantA cons_a
            xml.cantB cons_b
            xml.actor actor
            xml.fecha Time.now.inspect 
            xml.totalA cant_a - cons_a
            xml.totalB cant_b - cons_b
          end
        }.to_xml
        File.write("data.xml", xml)
      end
    end

  end

  def consultar(ts)
    if ts.nil?
      IO.read "data.xml"
    end
  end

  def respaldar
    data = IO.read("data.xml")
    socket = TCPSocket.new '192.168.0.103', 5000
    socket.puts JSON.dump({ content: data })
    result = socket.gets.chomp == "Success"
    socket.close
    result
  end

  def restaurar
    socket = TCPSocket.new '192.168.0.103', 5000
    socket.puts "RESTORE"
    msg = JSON.parse(socket.gets.chomp)
    IO.write("data.xml", msg["content"])
    socket.close
  end

  private 
  def value_of (xml, path)
    xml.xpath(path).children.count != 0 ? Integer(xml.xpath(path).children.last.text) : 0
  end
end
