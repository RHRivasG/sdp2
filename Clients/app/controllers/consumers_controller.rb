class ConsumersController < ApplicationController
  def show
    @consumer_class = 'active'
  end

  def create
    render xml: { Hello: 'World' }, status: :ok
  end
end
