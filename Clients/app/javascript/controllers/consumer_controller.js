import { Controller } from "stimulus"
import axios from "axios"

export default class extends Controller {
  static targets = [ 'productsA', 'productsB' ]

  connect() {
    let token = document.querySelector("meta[name = 'csrf-token']").content
    this.httpClient = axios.create({
      baseURL: 'http://localhost:3000',
      headers: {
        'Content-Type': ['json', 'xml'],
        'X-CSRF-Token': token
      }
    })
  }

  async greet(e) {
    e.preventDefault()
    let operationResult =
        await this.httpClient.post('/consumer', {
          productsA: parseInt(this.targets.productsA.value),
          productsB: parseInt(this.targets.productsB.value)
        })
        .then(result => {
          let parser = new DOMParser()
          return parser.parseFromString(result.data, "text/xml")
        })
    console.log(operationResult)
  }
}
