Rails.application.routes.draw do
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  get "/producer", to: "client#producer"
  root "consumers#index"

  resource :consumer, only: [:show, :create]
  resolve('Consumer') { [:consumer] }
end
